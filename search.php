<?php get_header(); ?>
<section class="container">
	<h1><?php printf( __( 'Search Results for: %s' ), get_search_query() ); ?></h1>
	<article>search</article>
	<?php get_sidebar(); ?>
</section>
<?php get_footer(); ?>