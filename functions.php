<?php 

function kurz_init_scripts(){
	wp_enqueue_style( 'style', get_stylesheet_uri(),false,'1.0' );

	if ( esc_url( home_url() ) == 'http://localhost/kurz2' ) {		// ak je to v nasom kompe tak naco cakat na google :D
		$jqueryUrl = get_template_directory_uri() . '/js/jquery-1.7.2.min.js';
	} else {
		$jqueryUrl = 'http://code.jquery.com/jquery-1.7.1.min.js';
	}

	wp_deregister_script('jquery');		// zrusenie defaultneho jquery
	wp_register_script('jquery', $jqueryUrl, array(), false, false);
	wp_enqueue_script('jquery');		// pridanie nasho jquery

	wp_register_script('kurzscript', get_template_directory_uri() . '/js/kurz_lib.js', array('jquery'), false, false);
	wp_enqueue_script('kurzscript');	// vlastna kniznica javascriptu
}
add_action( 'wp_enqueue_scripts', 'kurz_init_scripts' );	// ked pride na rad nacitavanie skriptov tak zavola zadanu funkciu

function kurz_widgets_init() {
	register_sidebar( array(
		'name'			=> __( 'Main sidebar', 'kurz' ),
		'id'			=> 'main-sidebar',
		'description'	=> __( 'Bar on the right side' ),
		'before_widget'	=> '',
		'before_title'	=> '<h2 class="widgetTitle">',
		'after_title'	=> '</h2><div class="widgetBody">',
		'after_widget'	=> '</div>'
	) );
	register_sidebar( array(
		'name'			=> __( 'Footer', 'kurz' ),
		'id'			=> 'footer-bar',
		'description'	=> '',
		'before_widget'	=> '',
		'before_title'	=> '<h2 class="widgetTitle">',
		'after_title'	=> '</h2><div class="widgetBody">',
		'after_widget'	=> '</div>'
	) );
}
add_action( 'widgets_init', 'kurz_widgets_init' );
 ?>